#!/usr/bin/env python

from distutils.core import setup

setup(name="pysnmp",
      version="2.0.9",
      description="Python SNMP Toolkit",
      author="Ilya Etingof",
      author_email="ilya@glas.net ",
      url="http://sourceforge.net/projects/pysnmp/",
      packages=['pysnmp'],
      license="BSD"
      )
